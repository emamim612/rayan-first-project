import { useContext, useEffect, useState } from 'react';
import Menu from '@mui/material/Menu';
import { CircularProgress, IconButton, styled, TextField } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { useUpdatePost } from '../../api/hooks';

import { DataContext } from '../../store/GlobalState';
import { AppSetNotify } from '../../store/Actions';



const CardBody = (props) => {
    const { id, updatePost, isLoading } = props
    const initialState = { title: props.title, body: props.body }
    const [postData, setPostData] = useState(initialState)
    const { title, body } = postData
    const handleChangeInput = e => {
        const { name, value } = e.target
        return setPostData({ ...postData, [name]: value })
    }
    return (<>
        <CardContent sx={{ display: 'flex', flexDirection: 'column' }}>
            <TextField
                sx={{ marginBottom: '10px' }}
                multiline
                rows={3}
                label="title"
                value={title}
                name='title'
                onChange={handleChangeInput}

            />
            <TextField
                multiline
                rows={6}
                label="body"
                value={body}
                name='body'
                onChange={handleChangeInput}
            />

        </CardContent>
        <CardActions sx={{ padding: '10px 22px' }}>
            <Button onClick={() => updatePost({id, postData})} variant='contained' size="small">
                {isLoading ? <CircularProgress sx={{ color: 'white' }} size={20} /> : 'Update'}
            </Button>
        </CardActions>
    </>
    )
};



const EditColumnsContainer = styled('div')({
    width: '500px',
    // padding: '10px'
})

export default function EditColumn(props) {
    const [anchorEl, setAnchorEl] = useState(null);

    const { dispatch } = useContext(DataContext)

    const { mutate: updatePost, isSuccess, isLoading } = useUpdatePost()

    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    useEffect(() => {
        if (isSuccess) {
            dispatch(AppSetNotify('Successfully updated', 'success'))
            handleClose()
        }
    }, [isSuccess])

    return (
        <div>
            <IconButton
                id="basic-button"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
            >
                <EditIcon color='info' />
            </IconButton>
            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                sx={{
                    left: '0',
                    top: '0',
                    '& div': {
                        '& ul': {
                            paddingTop: 0,
                            paddingBottom: 0,
                        }
                    }

                }}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <Box sx={{ minWidth: 350 }}>
                    <Card variant="outlined"><CardBody updatePost={updatePost} isLoading={isLoading} {...props} /></Card>
                </Box>
            </Menu>
        </div>
    );
}