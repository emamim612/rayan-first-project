import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import { useDeletePost, useGetAllPosts } from '../../api/hooks';
import EditColumn from './EditColumn';
import { DataContext } from '../../store/GlobalState';
import { AppSetNotify } from '../../store/Actions';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));



export default function CustomizedTables() {
    const { dispatch } = React.useContext(DataContext)
    const { data } = useGetAllPosts()
    const [displayedData, setDisplayedData] = React.useState(data)
    const {mutate: deletePost, isSuccess: deleteSuccess} = useDeletePost()

    React.useEffect(() => {
        if(deleteSuccess) dispatch(AppSetNotify('Successfully Deleted', 'success'))
    }, [deleteSuccess])
    React.useEffect(() => {
        if(data ) setDisplayedData(data.splice(0, 20))
    }, [data])
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell>#</StyledTableCell>
                        <StyledTableCell align="left">Title</StyledTableCell>
                        <StyledTableCell align="left">Body</StyledTableCell>
                        <StyledTableCell align="left">Actions</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {displayedData && displayedData.map((row) => (
                        <StyledTableRow key={row.id}>
                            <StyledTableCell align="left">
                                {row.id}
                            </StyledTableCell>
                            <StyledTableCell align="left">{row.title}</StyledTableCell>
                            <StyledTableCell align="left">{row.body}</StyledTableCell>
                            <StyledTableCell sx={{ display: 'flex', alignItems: 'center'}} align="left">
                                <EditColumn {...row}/>
                                <IconButton onClick={() => deletePost(row.id)} color='error'>
                                    <DeleteIcon />
                                </IconButton>
                            </StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}