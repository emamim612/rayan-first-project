import { useMutation, useQuery, useQueryClient } from "react-query";
import PostsService, { allPostsKey } from "./axios";

export function useGetAllPosts(
) {
    return useQuery(
        allPostsKey
        ,
        () => PostsService.getAllPosts(),
        { keepPreviousData: false, enabled: true }
    );
}
export function useDeletePost() {
    const queryClient = useQueryClient()
    return useMutation(
        PostsService.deletePost
        ,
        {
            onMutate: (id) => {
            },
            onSuccess: () => {
               queryClient.invalidateQueries(allPostsKey)
            }
        }
    );
}
export function useUpdatePost() {
    const queryClient = useQueryClient()
    return useMutation(
        PostsService.updatePost
        ,
        {
            onMutate: ({id, postData}) => {
            },
            onSuccess: () => {
               queryClient.invalidateQueries(allPostsKey)
            }
        }
    );
}