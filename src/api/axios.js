import axios from "axios"

const apiClient = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com/',
    headers: {
        "Content-type": "application/json",
    },
});

export const allPostsKey = 'posts'
const getAllPosts = async () => {
  const response = await apiClient.get('posts');
  return response.data;
}
const deletePost = async (id) => {
  const response = await apiClient.delete(`posts/${id}`);
  return response.data;
}
const updatePost = async ({id, postData}) => {
  const response = await apiClient.patch(`posts/${id}`, postData);
  return response.data;
}

const PostsService = {
    getAllPosts,
    deletePost,
    updatePost
  }
  export default PostsService;