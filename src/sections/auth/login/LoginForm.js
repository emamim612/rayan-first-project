import { useContext, useState } from 'react';
// @mui
import { Link, Stack, IconButton, InputAdornment, TextField, Checkbox, FormControlLabel } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// components
import Iconify from '../../../components/iconify';
import { DataContext } from '../../../store/GlobalState';
import { AppSetAuth } from '../../../store/Actions';

// ----------------------------------------------------------------------

export default function LoginForm() {
  const { dispatch } = useContext(DataContext)

  const initialState = { email: '', password: '', rememberMe: false }

  const [userData, setUserData] = useState(initialState)

  const { email, password, rememberMe } = userData

  const handleChangeInput = e => {
    const { name, value, type, checked } = e.target
    if (type === 'checkbox') return setUserData({ ...userData, [name]: checked })
    return setUserData({ ...userData, [name]: value })
  }

  const [showPassword, setShowPassword] = useState(false);

  const handleSubmit = () => {
    return AppSetAuth(email, password, rememberMe, dispatch)
  };

  return (
    <>
      <Stack spacing={3}>
        <TextField name="email" label="Email address" value={email} onChange={handleChangeInput} />

        <TextField
          name="password"
          label="Password"
          value={password} onChange={handleChangeInput}
          type={showPassword ? 'text' : 'password'}

          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                  <Iconify icon={showPassword ? 'eva:eye-fill' : 'eva:eye-off-fill'} />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Stack>

      <Stack direction="row" alignItems="center" justifyContent="space-between" sx={{ my: 2 }}>
        <FormControlLabel
          control={<Checkbox
            name="rememberMe"
            onChange={handleChangeInput}
            checked={rememberMe}
          />}
          label="Remember me" />
        <Link variant="subtitle2" underline="hover">
          Forgot password?
        </Link>
      </Stack>

      <LoadingButton fullWidth size="large" type="submit" variant="contained" onClick={handleSubmit}>
        Login
      </LoadingButton>
    </>
  );
}
