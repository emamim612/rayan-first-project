import { createContext, useReducer, useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom';
import { ACTIONS } from './Actions'
import reducers from './Reducers'


export const DataContext = createContext()

const loginPath = '/login'
const redirectPath = '/dashboard/app'


export const DataProvider = ({children}) => {
    const [previeousPath, setPrevieousPath] = useState(redirectPath)
    const initialState = { 
        rtl: false,
        notify: false,
        login: false,
        rememberMe: false
    }

    const navigate = useNavigate()

    const location = useLocation();

    const [state, dispatch] = useReducer(reducers, initialState)

    const { rtl, rememberMe, login } = state

    useEffect(() => {
        if(location.pathname !== loginPath) setPrevieousPath(location.pathname)
        const rtl = localStorage.getItem("__rayan__project__rtl");
        const rememberMe = localStorage.getItem("__rayan__project__rememberMe");

        if(rtl === 'true') dispatch({ type: ACTIONS.TOGGLE_DIRECTION, payload: true })
        if(rememberMe === 'true') dispatch({ type: ACTIONS.LOGIN, payload: {
            login: true,
            rememberMe: true
        } })
    }, [])

    useEffect(() => {
        localStorage.setItem('__rayan__project__rtl', rtl)
    }, [rtl])

    useEffect(() => {
        localStorage.setItem('__rayan__project__rememberMe', rememberMe)
    }, [rememberMe])

    useEffect(() => {
        if(location.pathname === loginPath && login) navigate(previeousPath, { replace: true });
        if(location.pathname !== loginPath && !login) navigate(loginPath, { replace: true });
    }, [login])

    useEffect(() => {
        if(location.pathname !== loginPath && !login) navigate(loginPath, { replace: true });
    }, [location.pathname])

    return(
        <DataContext.Provider value={{state, dispatch}}>
            {children}
        </DataContext.Provider>
    )
}