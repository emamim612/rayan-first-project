const { ACTIONS } = require("./Actions");

const reducers = (state, action) => {
    switch(action.type){
        case ACTIONS.TOGGLE_DIRECTION:
            return {
                ...state,
                rtl: action.payload
            };
        case ACTIONS.NOTIFY:
            return {
                ...state,
                notify: action.payload
            };
        case ACTIONS.LOGIN:
            return {
                ...state,
                login: action.payload.login,
                rememberMe: action.payload.rememberMe,
            };
        case ACTIONS.LOGOUT:
            return {
                ...state,
                login: false,
                rememberMe: false,
            };
        default:
            return state;
    }
}

export default reducers