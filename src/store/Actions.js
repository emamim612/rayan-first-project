export const ACTIONS = {
    TOGGLE_DIRECTION: 'TOGGLE_DIRECTION',
    NOTIFY: 'NOTIFY',
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT'
}

export const AppSetNotify = (message, severity) => {
    if (message) {
        return {
            type: ACTIONS.NOTIFY,
            payload: {
                message, severity
            }
        }
    }
    return {
        type: ACTIONS.NOTIFY,
        payload: {}
    }

}
export const AppSetAuth = (email, password, rememberMe, dispatch) => {
    if (email !== 'abc@mail.com' || password !== '123456') return dispatch(AppSetNotify('invalid credentials', 'error'))
    return dispatch({
        type: ACTIONS.LOGIN,
        payload: {
            rememberMe,
            login: true
        }
    })
}