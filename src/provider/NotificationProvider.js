import React, { useContext } from 'react'
import { Alert, Snackbar } from '@mui/material'
import { DataContext } from '../store/GlobalState'
import { AppSetNotify } from '../store/Actions'

const NotificationProvider = () => {
    const { state, dispatch } = useContext(DataContext)
    const { notify } = state
    const open = Boolean(notify.message)
    const handleClose = () => {
        dispatch(AppSetNotify())
    }
    return (
        <Snackbar open={open} autoHideDuration={3000} onClose={handleClose} anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
            <Alert onClose={handleClose} severity={notify?.severity} sx={{ width: '100%' }}>
            {notify?.message}
            </Alert>
        </Snackbar>
    )
}

export default NotificationProvider