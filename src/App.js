import { QueryClient, QueryClientProvider } from "react-query";
// routes
import Router from './routes';
// theme
import ThemeProvider from './theme';
// components
import ScrollToTop from './components/scroll-to-top';
import { StyledChart } from './components/chart';
import { DataProvider } from './store/GlobalState';
import NotificationProvider from './provider/NotificationProvider';

// ----------------------------------------------------------------------

export default function App() {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <DataProvider>
        <ThemeProvider>
          <NotificationProvider />
          <ScrollToTop />
          <StyledChart />
          <Router />
        </ThemeProvider>
      </DataProvider>
    </QueryClientProvider>
  );
}
