import { useContext } from 'react'
import { FormControlLabel } from '@mui/material'
import { DataContext } from '../../../store/GlobalState'
import { ACTIONS } from '../../../store/Actions'
import { Android12Switch } from '../../../components/input/switch'


const DirectionSwitch = () => {
    const {state, dispatch} = useContext(DataContext)
    const { rtl } = state
    return (
        <FormControlLabel
            sx={{
                color: 'black'
            }}
            control={<Android12Switch />}
            onChange={() => dispatch({type: ACTIONS.TOGGLE_DIRECTION, payload: !rtl})}
            checked={rtl}
            label="Direction RTL"
        />
    )
}

export default DirectionSwitch